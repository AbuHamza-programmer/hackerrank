#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    
    int n;
    cin >> n;
    vector<int> v (n,0);
    
    for(int i=0;i<n;i++)
        cin >> v[i];
    
    int q, a, b;
    
    cin >> q >> a >> b;
    
    v.erase(v.begin()+q-1);
    v.erase(v.begin()+a-1,v.begin()+b-1);
    
    cout<<v.size()<<endl;
    for(int i=0;i<v.size();i++)
        cout<< v[i]<<" ";
    
    cout<<endl;
    
    
    return 0;
}
