#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <set>
#include <map>
#include <algorithm>
using namespace std;


int main() {
    int n,t,g;
    string name;
    map<string,int>m;
    cin>>n;
    while(n--)
        {
        cin>>t>>name;
        if(t==1)
            {cin>>g;
             m[name]+=g;
            }
        else if(t==2)
            {
            m.erase(name);
        }
        else 
            cout<<m[name]<<endl;
    }
    return 0;
}
