#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    int n,q,num,index;
    cin>>n;
    vector<int> v(n);
    
    for(int i=0;i<n;i++)
        cin>>v[i];
    
    cin>>q;
    vector<int>::iterator low;
    while(q--)
        {
        cin>>num;
        low=std::lower_bound (v.begin(), v.end(), num); 
        index=(low- v.begin());
        if(v[index]==num)cout << "Yes " << index + 1<< '\n';
        else cout << "No " << index + 1<< '\n';
    }
    return 0;
}
