#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <set>
#include <algorithm>
using namespace std;


int main() {
    int q,t,n;
    
    set<int> s;
    cin>>q;
    while(q--)
        {
        cin>>t>>n;
        if(t==1)s.insert(n);
        else if(t==2)s.erase(n);
        else 
            {
            if(s.find(n)==s.end())cout<<"No"<<endl;
            else cout<<"Yes"<<endl;
        }
    }
    return 0;
}
