#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    int N,n,k,q,i,j;
    
    cin>>N>>q;
    vector <vector<int> >v(N);
    //read data
    for(i=0;i<N;i++)
        {
          cin>>k;
        v[i].resize(k);
        for(j=0;j<k;j++)
            {
            cin>>v[i][j];
        }
    }
    

    while(q--)
        {
        cin>>i>>j;
        cout<<v[i][j]<<endl;
    }
    
    
    return 0;
}
