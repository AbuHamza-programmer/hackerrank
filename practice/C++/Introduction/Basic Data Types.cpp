#include <iostream>
#include <cstdio>
using namespace std;

int main() {
    // Complete the code.
    int x;
    long y;
    char c;
    float f;
    double d;
    scanf("%d %ld %c %f %lf", &x, &y, &c, &f, &d);
    printf("%d\n%ld\n%c\n%f\n%lf", x, y, c, f, d);

    return 0;
}
