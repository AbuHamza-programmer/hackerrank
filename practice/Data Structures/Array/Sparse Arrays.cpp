#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    int N,Q;
    string s;
    cin>>N;
    vector<string>v(N);
    for(int i=0;i<N;i++)
        cin>>v[i];
    cin>>Q;
    while(Q--)
    {
        cin>>s;
        cout<<count(v.begin(), v.end(), s)<<endl;
    }
    return 0;
}
