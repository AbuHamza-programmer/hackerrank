#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <map>
using namespace std;


int main() {
    
    
    int N;
    cin>>N;
    map<string, string> contacts;
    string name, number;
    map<string,string>::iterator it;
    while(N--)
        {
        cin>>name>>number;
        contacts[name]=number;
    }
    while(!cin.eof())
        {
        cin>>name;
        it=contacts.find(name);
        if(it==contacts.end())
            cout<<"Not found"<<endl;
        else
            cout<<name<<"="<< it->second <<endl;
        
    }
    
    return 0;
}
