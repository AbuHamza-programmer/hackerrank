from sys import stdin

N=int(raw_input())
d={}

for _ in range(N):
    name,number=raw_input().split()
    d[name]=number

lines=stdin.read().splitlines()    

for name in lines:
    if name in d:
        print name+"="+d[name]
    else:
        print "Not found"  