def get_total_cost_of_meal():
    # original meal price
    meal_cost = float(raw_input())
    # tip percentage
    tip_percent = int(raw_input())
    # tax percentage
    tax_percent = int(raw_input())

    

    # cast the result of the rounding operation to an int and save it as total_cost 
    total_cost = int(round( meal_cost*(1+((tip_percent+tax_percent )/100.0)) ))

    return str(total_cost)

# Print your result
print("The total meal cost is " + get_total_cost_of_meal() + " dollars.")