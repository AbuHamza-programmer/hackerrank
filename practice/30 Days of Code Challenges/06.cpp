#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    
    int T;
    cin>>T;
    string s;
    getline(cin,s);//first endl 
    while(T--)
        {
        getline(cin,s);
        for(int i=0;i<s.length();i+=2)          
            cout<<s[i];
        
        cout<<" ";
        
        for(int i=1;i<s.length();i+=2)          
            cout<<s[i];
        
        cout<<endl;
    }
    return 0;
}