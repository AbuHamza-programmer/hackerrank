#!/bin/python

import sys
import re

N = int(raw_input().strip())
names = []
for a0 in xrange(N):
    firstName,emailID = raw_input().strip().split(' ')
    firstName,emailID = [str(firstName),str(emailID)]
    
    matchObj = re.match( r'(\w|.)+@gmail.com', emailID)

    if matchObj:
        names.append(firstName)

names.sort()
for name in names:
    print name
