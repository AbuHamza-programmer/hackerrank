#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */   
    
    float mealCost;
    int tipPercent, taxPercent;
    
    cin >> mealCost >> tipPercent >> taxPercent;
    
    float totalCost=mealCost*(1+((tipPercent+taxPercent)/100.0));
    cout<<"The total meal cost is "<<round(totalCost)<<" dollars."<<endl;
    return 0;
}
