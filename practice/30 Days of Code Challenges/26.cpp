#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    int act_d, act_m, act_y;
    int due_d, due_m, due_y;
    
    cin >> act_d >> act_m >> act_y;
    cin >> due_d >> due_m >> due_y;
    
    if(act_y > due_y) cout<<10000<<endl;
    else if(act_y == due_y && act_m > due_m) cout<< 500 * (act_m - due_m)<<endl;
    else if(act_y == due_y && act_m == due_m && act_d > due_d) cout<< 15 * (act_d - due_d)<<endl;
    else cout<<0<<endl;    
        
    return 0;
}
