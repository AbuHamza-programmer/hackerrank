#include <map>
#include <set>
#include <list>
#include <cmath>
#include <ctime>
#include <deque>
#include <queue>
#include <stack>
#include <string>
#include <bitset>
#include <cstdio>
#include <limits>
#include <vector>
#include <climits>
#include <cstring>
#include <cstdlib>
#include <fstream>
#include <numeric>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <unordered_map>

using namespace std;

int max_ones(int x)
    {
    int count=0;
    while(x!=0)
        {
        count++;
        x=x&(x << 1);
    }
    return count;
}
int main(){
    int n;
    cin >> n;
    cout<<max_ones(n)<<endl;
    return 0;
}
