# class Node:
#     def __init__(self,data):
#         self.data = data
#         self.next = None
# class Solution:
#     def insert(self,head,data):
#         p = Node(data)        
#         if head==None:
#             head=p
#         elif head.next==None:
#             head.next=p
#         else:
#             start=head
#             while(start.next!=None):
#                  start=start.next
#             start.next=p
#         return head 
#     def display(self,head):
#         current = head
#         while current:
#             print current.data,
#             current = current.next  


    def removeDuplicates(self,head):
        if head is None: return head
        
        currentNode = head
        nextNode = currentNode.next
        
        while nextNode:
            if currentNode.data == nextNode.data:
                nextNode = nextNode.next
                currentNode.next = nextNode
            else:
                nextNode = nextNode.next
                currentNode = currentNode.next
      
  
        return head
  
  
  
  

# mylist= Solution()
# T=int(input())
# head=None
# for i in range(T):
#     data=int(input())
#     head=mylist.insert(head,data)   
# head=mylist.removeDuplicates(head)
# mylist.display(head);