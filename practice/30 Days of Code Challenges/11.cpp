#include <map>
#include <set>
#include <list>
#include <cmath>
#include <ctime>
#include <deque>
#include <queue>
#include <stack>
#include <string>
#include <bitset>
#include <cstdio>
#include <limits>
#include <vector>
#include <climits>
#include <cstring>
#include <cstdlib>
#include <fstream>
#include <numeric>
#include <sstream>
#include <iostream>
#include <algorithm>
using namespace std;

inline int hourglass(const vector< vector<int> > & v, int i, int j)
    {
    return v[i][j]+v[i-1][j-1]+v[i-1][j]+v[i-1][j+1]+v[i+1][j-1]+v[i+1][j]+v[i+1][j+1];
}


int main(){
    vector< vector<int> > arr(6,vector<int>(6));
    for(int arr_i = 0;arr_i < 6;arr_i++){
       for(int arr_j = 0;arr_j < 6;arr_j++){
          cin >> arr[arr_i][arr_j];
       }
    }
    int maximum=-100;
    for(int i=1;i<=4;i++)
        for (int j=1;j<=4;j++)
        {
             maximum=max(maximum,hourglass(arr,i,j));   
    }
    
    cout<<maximum<<endl;
    
    return 0;
}