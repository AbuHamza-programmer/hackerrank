import math 

def prime(x):
    
    if x==1 : return False
    if x==2 : return True

    if (x & 1)==0 : return False #all even numbers
    for i in range(2, 1+int(math.sqrt(x))):
        if (x%i) ==0: 
            return False
    
    return True

n = int(raw_input())
for _ in range(n):
    x = int(raw_input())
    if prime(x): print "Prime"
    else: print "Not prime"