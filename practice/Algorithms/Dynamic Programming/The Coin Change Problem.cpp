#include <map>
#include <set>
#include <list>
#include <cmath>
#include <ctime>
#include <deque>
#include <queue>
#include <stack>
#include <string>
#include <bitset>
#include <cstdio>
#include <limits>
#include <vector>
#include <climits>
#include <cstring>
#include <cstdlib>
#include <fstream>
#include <numeric>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <unordered_map>

using namespace std;
vector<vector<long > > dp;
//for an amount of money for each coin, see how many ways you can build money using(and not using) coin
//for each coin, see the number of ways to use it to make an amount of money.
//if we will not use it, we will skip it and go to the next coin


//the number of ways as well as the dp array should be long
long algo(vector<int> & coins,  int index, int money)
{
    if(money < 0) return 0; //can't make negative money
    if(money == 0)return 1;//no coins
    
    if(index == coins.size()) return 0;//no more coins and of course money>0
    if(dp[index][money]!=0)return dp[index][money];
   
    long ways = algo(coins,index+1, money)+ algo(coins,index, money-coins[index]);//leave  it or take it
        
    return dp[index][money] = ways;
    
}

long  make_change(vector<int>  coins,  int money) {
    
   return algo(coins, 0, money);//to make money using coins starting from 0
    
}



void init(int n, int m)
{
    dp.resize(m+1);
    for(int i=0; i<m+1; i++)
    {
        dp[i] = vector<long>(n+1, 0);
    }
}

int main(){
    int n;
    int m;
    cin >> n >> m;
    vector<int> coins(m);
    for(int coins_i = 0;coins_i < m;coins_i++){
       cin >> coins[coins_i];
    }
    init(n,m);

    
    cout << make_change(coins, n) << endl;
    return 0;
}
