#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main(){
    int t;
    int h=1;
    cin >> t;
    for(int a0 = 0; a0 < t; a0++){
        h=1;
        int n;
        cin >> n;
        
        for(int i=1;i<=n;i++)
            {
            if(i%2)h*=2;
            else h++;
        }
        
        cout<<h<<endl;
    }
    return 0;
}
