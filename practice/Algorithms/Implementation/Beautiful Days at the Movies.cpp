#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <sstream>
using namespace std;

int reversed(int x)
{
   int y=0;
    while(x!=0)
        {
        y=y*10+(x%10);
        x=x/10;
    }
    return y;
}
int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */
    
     int a,b,k;
    cin>>a>>b>>k;
    int count=0;
    for( int i=a;i<=b;i++)
        {
        //cout<<i<<" "<<reversed(i)<<endl;
        if((abs((i-reversed(i)))%k)==0)count++;

    }
cout<<count<<endl;

    return 0;
}
