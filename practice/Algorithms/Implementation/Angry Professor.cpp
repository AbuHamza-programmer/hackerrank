#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

int main(){
    int t;
    cin >> t;
    for(int a0 = 0; a0 < t; a0++){
        int n;
        int k;
        cin >> n >> k;
        int x;
        int count=0;
        for(int a_i = 0;a_i < n;a_i++){
           cin >> x;
            if(x<=0)count++;//number of students who arrived in time.
        }
        if(count<k)cout<<"YES"<<endl;
        else cout<<"NO"<<endl;
    }
    return 0;
}
