#include <map>
#include <set>
#include <list>
#include <cmath>
#include <ctime>
#include <deque>
#include <queue>
#include <stack>
#include <string>
#include <bitset>
#include <cstdio>
#include <limits>
#include <vector>
#include <climits>
#include <cstring>
#include <cstdlib>
#include <fstream>
#include <numeric>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <unordered_map>

using namespace std;


int main(){
    int n;
    cin >> n;
    vector<int> c(n);
    for(int c_i = 0;c_i < n;c_i++){
       cin >> c[c_i];
    }
    
    vector<int> count (100,0);//100 colors. count how many times, each color was found
    for(int i=0;i<n;i++)
        {
        count[c[i]-1]++;//it will not pass all cases if I remove the -1
    }
    int total=0;
    for(int i=0;i<100;i++)
        {
        total+=count[i]/2;
    }
    
    cout<<total<<endl;
    return 0;
}
