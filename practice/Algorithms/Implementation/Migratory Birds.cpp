#include <bits/stdc++.h>

using namespace std;

int main(){
    int n;
    cin >> n;
    vector<int> types(n);
    for(int types_i = 0; types_i < n; types_i++){
       cin >> types[types_i];
    }
    // your code goes here
    
    //count how many each of the occurs 
     vector<int> count(5,0);
    for(int i=0;i<n;i++)
        {
        count[types[i]-1]++;
    }
    //find first max
    int max=0;
    for(int i=0;i<5;i++)
        { 
        if(count[i]>count[max])max=i;
    }
    cout<<max+1<<endl;
    return 0;
}
