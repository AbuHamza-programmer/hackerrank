#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <vector> 
#include <list>
#include <queue>
#include <set>
#include <map>
#include <string>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <functional>
#include <numeric>
#include <cstdlib>
#include <cmath>
#include <cstdio>
#include <fstream>
#include <ctime>
#include <cassert>
#include <unordered_map>
using namespace std; // }}}

unordered_map<int, int> representatives;

inline int getRepresentativeOf(int v)
    {
    int x=v;
    if(representatives.find(v) == representatives.end()) 
        return representatives[v] = v;// initially each element represents itself as one seprate class
   
    while(x != representatives[x])
    { 
        representatives[x] = representatives[representatives[x]];
        x = representatives[x];
    }
    return representatives[v] = x;
}
inline void addEdge(int x, int y)
    {
    int i = getRepresentativeOf(x);
    int j = getRepresentativeOf(y);
    representatives[i] = j;
}
inline void update()//union find algorithm
    {
    unordered_map<int, int>::iterator it;
    for (it = representatives.begin(); it != representatives.end(); it++)
        {
        it->second = getRepresentativeOf(it->first); 
    }
}


int main()
{ 
    //all connected nodes are the same. have the same representitive
    int N, I;
    cin >> N >> I;
    vector<vector<int> > pairs(N);
    for (int i = 0; i < I; ++i) {
        int a, b;
        cin >> a >> b;
        addEdge(a,b);
    }
    
    update();//merge
  
    //add all representitives to a vector
    vector<int> v;
for(int i=0; i<N; i++) {
  v.push_back(getRepresentativeOf(i) );//we use the function getRepresentativeOf and not the map representatives, because the map only contains vertices which are connected to other vertices. it doesn't contain single vertices

}


    sort(v.begin(), v.end());
    //find frequency for each and multuply all of them
    
    int count = 1;
    vector<int>histogram;
   
    for(int i=1;i<v.size();i++)
    {
        if(v[i] == v[i-1])
        {count++;}
        else
        {histogram.push_back(count); count=1;}
    }
    histogram.push_back(count);
    // [2, 3, 4, 1, 1, 3] first country has 2, second country has 3 and so on
    
    //now for each pair of countries we will multiply the number of people form the two countires and add that to the total
   //cout<<"Hist: ";
    vector<int> partialSum=histogram;
    for (int i=1;i<histogram.size(); i++)
    {
        partialSum[i]+=partialSum[i-1];
        //cout<<histogram[i]<<" ";
    }
    //cout<<endl;
   long long result = 0;
    for(int i=0; i<histogram.size(); i++)
    {
        if(histogram[i]==1)result+=N-partialSum[i]; //optimization step to reduce runtime. if it is 1, it can be combined with any follwing number
        else
        {       
            for(int j=i+1;j<histogram.size(); j++)
            result+=histogram[i]*histogram[j];
        }
    }
    
    /** Write code to compute the result using N,I,pairs **/
    

    
    cout << result << endl;
    return 0;
}
