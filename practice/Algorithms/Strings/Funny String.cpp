#include <bits/stdc++.h>

using namespace std;

string funnyString(string s){
    int n=s.length();
    for(int i=0;i<n/2;i++)
    {
        if(abs(s[i]-s[i+1]) != abs(s[n-i-1]-s[n-i-2])) return "Not Funny";
    }
    return "Funny";
        
}

int main() {
    int q;
    cin >> q;
    for(int a0 = 0; a0 < q; a0++){
        string s;
        cin >> s;
        string result = funnyString(s);
        cout << result << endl;
    }
    return 0;
}
