#include <bits/stdc++.h>

using namespace std;

string super_reduced_string(string s){
    // Complete this function
    
    for(int i=0;i<s.length();i++)
    {
        if(s[i] == s[i+1])
        {s=s.erase(i,2); i=max(-1,i-2);}//subtract two positions, and i will be incremented in the next iteration.
    }
    if(s.length()==0)return "Empty String";
    return s;
}

int main() {
    string s;
    cin >> s;
    string result = super_reduced_string(s);
    cout << result << endl;
    return 0;
}
