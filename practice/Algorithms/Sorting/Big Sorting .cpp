#include <bits/stdc++.h>

using namespace std;

bool compare(string num1, string num2)
    {
        if(num1.length() < num2.length()) return true;
        if(num1.length() > num2.length()) return false;
        return num1 < num2;
    }


int main(){
    int n;
    cin >> n;
    vector<string> unsorted(n);
    for(int unsorted_i = 0; unsorted_i < n; unsorted_i++){
       cin >> unsorted[unsorted_i];
    }

    sort(unsorted.begin(), unsorted.end(), compare );
    
    for(int unsorted_i = 0; unsorted_i < n; unsorted_i++){
       cout<< unsorted[unsorted_i]<<endl;;
    }
    // your code goes here
    return 0;
}
