#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include<sstream>
using namespace std;

string two_digit(int n)
    {
    stringstream ss;
    ss<<n;
    if(n<10)return "0"+ss.str();
    return ss.str();
}
int main(){
    string time;
    cin >> time;
    
    stringstream ss(time);
    int h,m,s;
    char t;
    
    ss>>h>>t>>m>>t>>s>>t;
    
    if((t=='P')&& (h<12)) h+=12;
    else if((t=='A')&&(h==12))h=0;
        
    cout<<two_digit(h)<<":"<<two_digit(m)<<":"<<two_digit(s)<<endl;
    return 0;
}
