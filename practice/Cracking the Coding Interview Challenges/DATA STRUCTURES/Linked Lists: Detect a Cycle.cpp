/*
Detect a cycle in a linked list. Note that the head pointer may be 'NULL' if the list is empty.

A Node is defined as: 
    struct Node {
        int data;
        struct Node* next;
    }
*/

bool has_cycle(Node* head) {
    
    if(head==NULL)  return false;
    
    Node * p1=head;    //advance this pointer once        
    Node * p2=head;   //advance this pointer twice
    
    do
      {
        p2=p2->next; if(p2==NULL)return false; 
        p2=p2->next; if(p2==NULL)return false;
        
        p1=p1->next; 
    }
    while(p1!=p2); 
    
    
    return true;
}
