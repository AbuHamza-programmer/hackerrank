#include <map>
#include <set>
#include <list>
#include <cmath>
#include <ctime>
#include <deque>
#include <queue>
#include <stack>
#include <string>
#include <bitset>
#include <cstdio>
#include <limits>
#include <vector>
#include <climits>
#include <cstring>
#include <cstdlib>
#include <fstream>
#include <numeric>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <unordered_map>

using namespace std;

struct node
{
  node * children[26];
  long int count;
};

class trie
{
    node * root;
    
    public:
    trie()
    {
        root = new node();
        root->count =0;
        
    }
    void insert(string word)
    {
        node * current = root;
        current->count++;
        int index;
        for(int i = 0; i<word.length(); i++)
        {
            index = word[i]-'a';
            if(current->children[index] == NULL) current->children[index]=new node;
            current = current->children[index]; 
            current->count++;
        }
        
    }
    
    long int find(string prefix)
    {
        int index;
        node * current = root;
        for(int i = 0; i < prefix.length(); i++)
        {
            index = prefix[i]-'a';
            if(current->children[index] == NULL)return 0;
            current = current->children[index]; 
        }
        return current->count;
    }
    
    
};

int main(){
    int n;
    trie t;
    cin >> n;
    for(int a0 = 0; a0 < n; a0++){
        string op;
        string contact;
        cin >> op >> contact;
        if(op=="add")t.insert(contact);
        else cout<<t.find(contact)<<endl;
    }
    return 0;
}
