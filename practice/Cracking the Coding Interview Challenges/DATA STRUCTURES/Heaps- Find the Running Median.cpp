#include <map>
#include <set>
#include <list>
#include <cmath>
#include <ctime>
#include <deque>
#include <queue>
#include <stack>
#include <string>
#include <bitset>
#include <cstdio>
#include <limits>
#include <vector>
#include <climits>
#include <cstring>
#include <cstdlib>
#include <fstream>
#include <numeric>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <iomanip>
#include <unordered_map>

using namespace std;

int main(){
    
    //algorithm from here: https://stackoverflow.com/a/10657732
    priority_queue<int> maxHeap;
    priority_queue<int, vector<int>, greater<int> > minHeap;
    int n;
    cin >> n;
    vector<int> a(n);
    for(int a_i = 0;a_i < n;a_i++){
       cin >> a[a_i];
    }
    
    //first two numbers
    cout<<fixed<<setprecision(1)<<a[0]/1.0<<endl;
    cout<<(a[0] + a[1]) / 2.0 <<endl;
    
    //algorithm
    maxHeap.push(min(a[0], a[1]));  
    minHeap.push(max(a[0], a[1]));
    double median;
    for(int i = 2; i < a.size(); i++)
    {
        //add element
        if(a[i] < maxHeap.top())                        maxHeap.push(a[i]);
        else                                            minHeap.push(a[i]);
        
        //balance heaps
        if(maxHeap.size() > minHeap.size() + 1)         {minHeap.push(maxHeap.top()); maxHeap.pop();}
        else if(minHeap.size() > maxHeap.size() + 1)    {maxHeap.push(minHeap.top()); minHeap.pop();}
        
        //find median
        if(maxHeap.size() > minHeap.size())              median = maxHeap.top();
        else if(maxHeap.size() < minHeap.size())         median = minHeap.top();
        else                                             median = (maxHeap.top() + minHeap.top() ) / 2.0;
        
        cout<<median<<endl;
    }
    
    
    
    return 0;
}
