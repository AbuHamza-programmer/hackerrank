#include <map>
#include <set>
#include <list>
#include <cmath>
#include <ctime>
#include <deque>
#include <queue>
#include <stack>
#include <string>
#include <bitset>
#include <cstdio>
#include <limits>
#include <vector>
#include <climits>
#include <cstring>
#include <cstdlib>
#include <fstream>
#include <numeric>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <unordered_map>

using namespace std;

vector<int> table(38,-1);
void init()
    {
    table[1]=1;
    table[2]=2;
    table[3]=4;
}
int calculate(int n)
    {
    if(table[n]!=-1)return table[n];
   
    
   
    int v1=calculate(n-1);
    int v2=calculate(n-2);
    int v3=calculate(n-3);
    
    return table[n]=v1+v2+v3;
}
int main(){
    int s;
    cin >> s;
    init();
    for(int a0 = 0; a0 < s; a0++){
        int n;
        cin >> n;
        cout<<calculate(n)<<endl;
    }
    return 0;
}
