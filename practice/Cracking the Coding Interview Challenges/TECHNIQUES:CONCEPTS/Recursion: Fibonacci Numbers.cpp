#include <iostream>
#include <vector>
using namespace std;

 vector<int> table(40,-1);

int fibonacci(int n) {
    
    if(table[n]!=-1)return table[n];
    if(n==0 || n==1)return table[n]=n;
   
    
    return table[n]=fibonacci(n-1)+fibonacci(n-2);

}
int main() {
    int n;
    cin >> n;
    cout << fibonacci(n);
    return 0;
}
