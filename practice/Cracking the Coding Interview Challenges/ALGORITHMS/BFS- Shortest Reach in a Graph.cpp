#include <cmath>
#include <cstdio>
#include <vector>
#include <queue>
#include <iostream>
#include <algorithm>
using namespace std;


class Graph {
    vector<vector<int> > adj;
    public:
        Graph(int n) {
            adj.resize(n);

        }
        
        void print()
        {
            for(int i=0; i<adj.size(); i++)
            {
                cout<<i+1<<": ";
                for(int j=0; j<adj[i].size(); j++)
                {cout<<1+adj[i][j]<<" ";}
                
                cout<<endl;
            }
            
        }
    
        void add_edge(int u, int v) {
            adj[u].push_back(v);
            adj[v].push_back(u);
        }
    
        vector<int> shortest_reach(int start) {//bfs
//adjacency matrix exceeds limits
            vector<int> distances(adj.size(), -1);
            vector<bool> visited(adj.size(), false);
            queue<int> q;
                        
           
            visited[start]=true;
            distances[start]=0;
            q.push(start);
            
            
            int current, child;

            while(!q.empty())
            {
                current=q.front();
                q.pop();
    
                for(int j=0;j<adj[current].size();j++)//loop over its children
                {
                    child=adj[current][j];
                    if(! visited[child])
                    {
                        q.push(child);
                        visited[child]=true;
                        distances[child]=6+distances[current];
                    }
                }
            }
            
            
            
            return distances;
        }
    
};

int main() {
    int queries;
    cin >> queries;
        
    for (int t = 0; t < queries; t++) {
      
      int n, m;
        cin >> n;
        // Create a graph of size n where each edge weight is 6: 
        Graph graph(n);
        
        cin >> m;
        // read and set edges
        for (int i = 0; i < m; i++) {
            int u, v;
            cin >> u >> v;
            u--, v--;
            // add each edge to the graph
            graph.add_edge(u, v);
        }
       //graph.print();
      int startId;
        cin >> startId;
        startId--;
        // Find shortest reach from node s
        vector<int> distances = graph.shortest_reach(startId);

        for (int i = 0; i < distances.size(); i++) {
            if (i != startId) {
                cout << distances[i] << " ";
            }
        }
        cout << endl;
    }
    
    return 0;
}
