#include <map>
#include <set>
#include <list>
#include <cmath>
#include <ctime>
#include <deque>
#include <queue>
#include <stack>
#include <string>
#include <bitset>
#include <cstdio>
#include <limits>
#include <vector>
#include <climits>
#include <cstring>
#include <cstdlib>
#include <fstream>
#include <numeric>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <unordered_map>

using namespace std;

vector< vector<int> > grid;
vector< vector<bool> > visited;

template <typename T>
vector< vector<T> > make_2d(int n , int m){
    
    vector< vector<T> > grid(n,vector<T>(m,0));
    return grid;
}
bool inside(int i, int j){
    
    if (i < 0 || i >= grid.size() || j < 0 || j >= grid[0].size() ) return 0;
    return true;
}
int flood_fill(int i, int j){

    if(!inside(i,j))return 0;
    if(visited[i][j])return 0;
    if(grid[i][j]==0)return 0;
    
    visited[i][j] = true;

    return 1 + flood_fill(i-1, j-1)
             + flood_fill(i-1, j)
             + flood_fill(i-1, j+1)     
             + flood_fill(i, j-1)
             + flood_fill(i, j+1)              
             + flood_fill(i+1, j-1)
             + flood_fill(i+1, j)      
             + flood_fill(i+1, j+1); 
}
int get_biggest_region(vector< vector<int> >& grid) {
    
    int maximum = -1;
    
    for(int i=0;i < grid.size(); i++)
        for(int j=0;j < grid[0].size(); j++)
        {
        maximum = max(maximum, flood_fill(i,j));
    }
    return maximum;
}

int main(){
    int n;
    cin >> n;
    int m;
    cin >> m;
    grid=make_2d<int>(n , m);
    visited=make_2d<bool>(n , m);
    
    for(int grid_i = 0;grid_i < n;grid_i++){
       for(int grid_j = 0;grid_j < m;grid_j++){
          cin >> grid[grid_i][grid_j];
       }
    }
    cout << get_biggest_region(grid) << endl;
    return 0;
}
