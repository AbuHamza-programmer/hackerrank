#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

struct Player {
    string name;
    int score;
}; 

bool operator<(Player p1, Player p2)
    {
    if(p1.score == p2.score)return (p1.name < p2.name);
    return (p1.score > p2.score);
    
}
void comparator(vector<Player> & players) {
    
    sort(players.begin(), players.end());
    
}

int main() {
    
    int n;
    cin >> n;
    vector< Player > players;
    string name;
    int score;
    for(int i = 0; i < n; i++){
        cin >> name >> score;
        Player p1;
        p1.name = name, p1.score = score;
        players.push_back(p1);
    }
    
    comparator(players);
    
    for(int i = 0; i < players.size(); i++) {
        cout << players[i].name << " " << players[i].score << endl;
    }
    return 0;
}
