#include <map>
#include <set>
#include <list>
#include <cmath>
#include <ctime>
#include <deque>
#include <queue>
#include <stack>
#include <string>
#include <bitset>
#include <cstdio>
#include <limits>
#include <vector>
#include <climits>
#include <cstring>
#include <cstdlib>
#include <fstream>
#include <numeric>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <unordered_map>

using namespace std;

//do mearge sort and count in the merging step. count only if the smaller element is from the second half. and in this case add to the count the number of elements remaining in the first half, because this element will need to cross them all.

//we will use one helper array of the same size(initially a copy of the array) and we will alternate between the array and the helper array at each step.
//in place merging

long merge(vector<int> &array, vector<int> &helper, int start, int mid, int end)
{
    long count = 0;
        int first_half_index = start, second_half_index = mid + 1, target_index = start;
        while (first_half_index <= mid || second_half_index <= end) {
            if (first_half_index > mid) {
                array[target_index++] = helper[second_half_index++];
            } else if (second_half_index > end) {
                array[target_index++] = helper[first_half_index++];
            } else if (helper[first_half_index] <= helper[second_half_index]) {
                array[target_index++] = helper[first_half_index++];
            } else {
                array[target_index++] = helper[second_half_index++];
                count += mid + 1 - first_half_index;
            }
        }

        return count;
}
long merge_count(vector<int> &array, vector<int> &helper, int start, int end)
{
    if(start >= end)return 0;
    int mid = (start+end)/2;
    long count=0;
    count+= merge_count(helper, array, start, mid);
    count+= merge_count(helper, array, mid+1, end);
    count+= merge(array, helper, start, mid, end);
    
    return count;  
}
long long count_inversions(vector<int> array) {
 
    vector<int> helper = array;
    
    return merge_count(array, helper, 0, array.size()-1);
}

int main(){
    int t;
    cin >> t;
    for(int a0 = 0; a0 < t; a0++){
        int n;
        cin >> n;
        vector<int> arr(n);
        for(int arr_i = 0;arr_i < n;arr_i++){
           cin >> arr[arr_i];
        }
        cout << count_inversions(arr) << endl;
    }
    return 0;
}
