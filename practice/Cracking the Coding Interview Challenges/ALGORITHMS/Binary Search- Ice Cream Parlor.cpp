#include <bits/stdc++.h>
using namespace std;

class IceCream {    
    public: 
        int flavor; 
        int index;

        IceCream(int f, int i) {
            this->flavor=f;
            this->index=i;
       }
    
};

bool compare(IceCream one, IceCream two)
        {
        return (one.flavor < two.flavor);
    }
 
int binarySearch(int first, int last, vector<IceCream>& arr, int search) {
    
   int mid;
   while(true)
       {
    if(first > last)return -1;
    if(search== arr[first].flavor)  return arr[first].index;
    if(search== arr[last].flavor)   return arr[last].index;
    
    mid = (first+last)/2;
    
     if(search < arr[mid].flavor) last= mid-1;
    else if (search > arr[mid].flavor) first= mid+1;
    else return arr[mid].index;
        }
         
}

int main() {
    int t;
    int n, m;
    cin >> t;
    for(int test = 0; test < t; test++) {       
        cin >> m >> n;
        vector<IceCream> arr;
        arr.reserve(n); 

        for (int i = 0; i < n; i++) {
            int cost;
            cin >> cost;
            arr.push_back(IceCream(cost, i + 1));
        }

        sort(arr.begin(), arr.end(),compare);
        int firstIndex = 100000, secondIndex = 100000;
        for(int i = 0; i < n - 1 ; i++) {
            int search = m - arr[i].flavor;
            if(search >= arr[i].flavor) {
                int index = binarySearch( i + 1, n - 1, arr, search);
                if( index != -1 ) {
                    cout << min(arr[i].index, index) << " " << max(arr[i].index, index) << endl;
                    break;

                }
            }
        }

    }

}

