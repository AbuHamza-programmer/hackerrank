#include <bits/stdc++.h>

using namespace std;

//we have equivalence class since we have symmetry, transitivity, and of course reflexivity.
//we will replace all elemement of a class X with a representitive x
//then will find the length of the longest plaindrome substring

unordered_map<int, int> representatives;

inline int getRepresentativeOf(int v)
    {
    int x=v;
    if(representatives.find(v) == representatives.end()) 
        return representatives[v] = v;// initially each element represents itself as one seprate class
   
    while(x != representatives[x])
    { 
        representatives[x] = representatives[representatives[x]];
        x = representatives[x];
    }
    return representatives[v] = x;
}
inline void addEdge(int x, int y)
    {
    int i = getRepresentativeOf(x);
    int j = getRepresentativeOf(y);
    representatives[i] = j;
}
inline void update()//union find algorithm
    {
    unordered_map<int, int>::iterator it;
    for (it = representatives.begin(); it != representatives.end(); it++)
        {
        it->second = getRepresentativeOf(it->first); 
    }
}
//might need to  use the function getRepresentativeOf and not the map representatives, because the map only contains vertices which are connected to other vertices. it doesn't contain single vertices
int findLength(vector<int> & v, vector<int> & r)
    {

    //LCS of v and v reversed(v)
    //http://www.geeksforgeeks.org/dynamic-programming-set-4-longest-common-subsequence/

    int m = v.size();
    int n = r.size();
    
    int L[m+1][n+1];
  

   for (int i=0; i<=m; i++)
   {
     for (int j=0; j<=n; j++)
     {
       if (i == 0 || j == 0)
         L[i][j] = 0;  
       else if (v[i-1]==r[j-1])
         L[i][j]=L[i-1][j-1] + 1;
       else
         L[i][j]=max(L[i-1][j], L[i][j-1]);
     }
   }
    
   return L[m][n];
}

int main() {
    int n;
    int k;
    int m;
    cin >> n >> k >> m;
    for(int a0 = 0; a0 < k; a0++){
        int x;
        int y;
        cin >> x >> y;
        //we will give x and y the same representitive.
        //if one of them already has a representitive, it will be the representitive of both.
        //if not, one of them will be the representitive for both
        addEdge(x,y);
    }
    
    update();//merge
    //cout<<"--"<<getRepresentativeOf(6)<<endl;
    vector<int> a(m);
    vector<int> b(m);
    int dd;
    for(int a_i = 0; a_i < m; a_i++){
       cin >> a[a_i];       
       //replace each number by its representative
       a[a_i] = getRepresentativeOf( a[a_i] ); // using representatives[ a[a_i] ] is wrong here, because if  a number x never appeared as an edge before, its representative was not calcuated before and representatives[x] is 0, and several 0s will match and make a plaindrome. Or we could check first it exists in the map  representatives otherwise we call the function getRepresentativeOf
       b[m-1-a_i] = a[a_i];
    }
    //find length of longest plaindrome substring
    cout<<findLength(a,b)<<endl;
    return 0;
}
