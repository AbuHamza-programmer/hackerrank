#include <bits/stdc++.h>

using namespace std;

int patternCount(string & s, int start){
    
    int count = 0;
    int pos=0;
    while(true)
        {
        pos = s.find("10", pos);
        if(pos==-1)
            break;

        while(s[++pos]=='0');
        
        if(s[pos]=='1')count++;
    }
    
    return count;
}

int main() {
    int q;
    cin >> q;
    for(int a0 = 0; a0 < q; a0++){
        string s;
        cin >> s;
        int result = patternCount(s, 0);
        cout << result << endl;
    }
    return 0;
}
