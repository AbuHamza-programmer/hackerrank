#include <bits/stdc++.h>
#include<vector>
#include<algorithm>
using namespace std;

int find_index_smallest_element(vector<int> v)
    {
        return min_element(v.begin(), v.end()) - v.begin();
}

int find_index_second_smallest_element(vector<int> v, int index_smallest_element = -1)
    {
    if(v.size() < 2)return -1;
    
    if(index_smallest_element == -1) index_smallest_element = find_index_smallest_element(v);
    int index_second_smallest_element=0;
    if(index_smallest_element==0)index_second_smallest_element=1;
    
    for(int i=0;i<v.size();i++)
        {
        if(v[i] < v[index_second_smallest_element] && i != index_smallest_element)
            {
            index_second_smallest_element = i;
        }
    }
    return index_second_smallest_element;
 
}

int twinArrays(vector <int> ar1, vector <int> ar2){
    int ar1_1min = find_index_smallest_element(ar1);
    int ar2_1min = find_index_smallest_element(ar2);
    
    if(ar1_1min != ar2_1min) return  ar1[ar1_1min] + ar2[ar2_1min] ;
    
    int ar1_2min = find_index_second_smallest_element(ar1);
    int ar2_2min = find_index_second_smallest_element(ar2);
    
    
    return  min( ar1[ar1_2min] + ar2[ar2_1min] , ar1[ar1_1min] + ar2[ar2_2min] );
    
    return 0;
}

int main() {
    int n;
    cin >> n;
    vector<int> ar1(n);
    for(int ar1_i = 0; ar1_i < n; ar1_i++){
       cin >> ar1[ar1_i];
    }
    vector<int> ar2(n);
    for(int ar2_i = 0; ar2_i < n; ar2_i++){
       cin >> ar2[ar2_i];
    }
    int result = twinArrays(ar1, ar2);
    cout << result << endl;
    return 0;
}
